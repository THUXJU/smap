package com.xj.grad.xjmap.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.widget.ImageView;

import com.xj.grad.xjmap.R;


public class WelCame extends Activity implements AnimationListener {
	ImageView imageView=null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wel_came);
		 imageView=(ImageView) findViewById(R.id.ItemImg);
		AnimationSet animationSet=new AnimationSet(true);
		AlphaAnimation alphaAnimation=new AlphaAnimation(0, 1);
		alphaAnimation.setDuration(2000);
		animationSet.addAnimation(alphaAnimation);
		imageView.setAnimation(animationSet);
		animationSet.setAnimationListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.wel_came, menu);
		return true;
	}

	@Override
	public void onAnimationEnd(Animation arg0) {
		// TODO Auto-generated method stub
		Intent intent=new Intent(this, Main.class);
		startActivity(intent);
		this.finish();
	}

	@Override
	public void onAnimationRepeat(Animation arg0) {
		// TODO Auto-generated method stub

	}

    @Override
    public void onAnimationStart(Animation arg0) {

        // TODO Auto-generated method stub

    }

}
