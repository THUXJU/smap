package com.xj.grad.xjmap.app;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.map.ItemizedOverlay;
import com.baidu.mapapi.map.MKEvent;
import com.baidu.mapapi.map.MapController;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.OverlayItem;
import com.baidu.mapapi.map.PoiOverlay;
import com.baidu.mapapi.map.PopupOverlay;
import com.baidu.mapapi.search.MKAddrInfo;
import com.baidu.mapapi.search.MKBusLineResult;
import com.baidu.mapapi.search.MKDrivingRouteResult;
import com.baidu.mapapi.search.MKPoiInfo;
import com.baidu.mapapi.search.MKPoiResult;
import com.baidu.mapapi.search.MKSearch;
import com.baidu.mapapi.search.MKSearchListener;
import com.baidu.mapapi.search.MKShareUrlResult;
import com.baidu.mapapi.search.MKSuggestionResult;
import com.baidu.mapapi.search.MKTransitRouteResult;
import com.baidu.mapapi.search.MKWalkingRouteResult;
import com.baidu.platform.comapi.basestruct.GeoPoint;
import com.xj.grad.xjmap.R;

public class Main extends Activity {
    BMapManager mBMapMan = null;
    MapView mMapView = null;
    MKSearch mMKSearch = null;
    EditText kw_text = null;
    double mLon1 = 87.624173;
    double mLon2 = 87.621792;
    double mLon3 = 87.62159;
    double mLon4 = 87.622354;
    double mLon5 = 87.620562;
    double mLon6 = 87.624685;
    double mLon7 = 87.625943;
    double mLon8 = 87.626401;
    double mLon9 = 87.62155;
    double mLon10 = 87.620934;
    double mLon11 = 87.622268;
    double mLon12 = 87.620113;
    double mLon13 = 87.622372;
    double mLon14 = 87.622879;
    double mLon15 = 87.619349;
    double mLon16 = 87.621074;
    double mLon17 = 87.621927;
    double mLon18 = 87.622893;
    double mLon19 = 87.629958;
    double mLon20 = 87.625943;
    double mLon21 = 87.634481;
    double mLon22 = 87.635846;
    double mLon23 = 87.628808;
    double mLon24 = 87.627326;
    double mLon25 = 87.629019;
    double mLon26 = 87.630389;
    double mLon27 = 87.634854;
    double mLon28 = 87.630614;
    double mLon29 = 87.632644;
    double mLon30 = 87.62782;
    double mLon31 = 87.589476;
    double mLon32 = 87.588717;
    double mLon33 = 87.59001;
    double mLon34 = 87.589754;
    double mLon35 = 87.588811;
    double mLon36 = 87.588959;
    double mLon37 = 87.584607;
    double mLon38 = 87.591951;
    double mLon00 = 87.636164;
    double mLon01 = 87.584607;
    double mLon02 = 87.622704;

    double mLat1 = 43.770766;
    double mLat2 = 43.773903;
    double mLat3 = 43.773135;
    double mLat4 = 43.773448;
    double mLat5 = 43.772152;
    double mLat6 = 43.772784;
    double mLat7 = 43.77268;
    double mLat8 = 43.770186;
    double mLat9 = 43.76964;
    double mLat10 = 43.771098;
    double mLat11 = 43.770291;
    double mLat12 = 43.770069;
    double mLat13 = 43.771182;
    double mLat14 = 43.77155;
    double mLat15 = 43.771023;
    double mLat16 = 43.77461;
    double mLat17 = 43.775153;
    double mLat18 = 43.774284;
    double mLat19 = 43.767169;
    double mLat20 = 43.773936;
    double mLat21 = 43.744021;
    double mLat22 = 43.745317;
    double mLat23 = 43.746307;
    double mLat24 = 43.745525;
    double mLat25 = 43.742083;
    double mLat26 = 43.741663;
    double mLat27 = 43.746163;
    double mLat28 = 43.746059;
    double mLat29 = 43.742399;
    double mLat30 = 43.744431;
    double mLat31 = 43.826763;
    double mLat32 = 43.826844;
    double mLat33 = 43.826916;
    double mLat34 = 43.827397;
    double mLat35 = 43.827543;
    double mLat36 = 43.827914;
    double mLat37 = 43.830945;
    double mLat38 = 43.827904;
    double mLat00 = 43.743995;
    double mLat01 = 43.826793;
    double mLat02 = 43.772078;

    GeoPoint p00 = new GeoPoint((int) (mLat00 * 1E6), (int) (mLon00 * 1E6));
    GeoPoint p01 = new GeoPoint((int) (mLat01 * 1E6), (int) (mLon01 * 1E6));
    GeoPoint p02 = new GeoPoint((int) (mLat02 * 1E6), (int) (mLon02 * 1E6));
    GeoPoint p1 = new GeoPoint((int) (mLat1 * 1E6), (int) (mLon1 * 1E6));
    GeoPoint p2 = new GeoPoint((int) (mLat2 * 1E6), (int) (mLon2 * 1E6));
    GeoPoint p3 = new GeoPoint((int) (mLat3 * 1E6), (int) (mLon3 * 1E6));
    GeoPoint p4 = new GeoPoint((int) (mLat4 * 1E6), (int) (mLon4 * 1E6));
    GeoPoint p5 = new GeoPoint((int) (mLat5 * 1E6), (int) (mLon5 * 1E6));
    GeoPoint p6 = new GeoPoint((int) (mLat6 * 1E6), (int) (mLon6 * 1E6));
    GeoPoint p7 = new GeoPoint((int) (mLat7 * 1E6), (int) (mLon7 * 1E6));
    GeoPoint p8 = new GeoPoint((int) (mLat8 * 1E6), (int) (mLon8 * 1E6));
    GeoPoint p9 = new GeoPoint((int) (mLat9 * 1E6), (int) (mLon9 * 1E6));
    GeoPoint p10 = new GeoPoint((int) (mLat10 * 1E6), (int) (mLon10 * 1E6));
    GeoPoint p11 = new GeoPoint((int) (mLat11 * 1E6), (int) (mLon11 * 1E6));
    GeoPoint p12 = new GeoPoint((int) (mLat12 * 1E6), (int) (mLon12 * 1E6));
    GeoPoint p13 = new GeoPoint((int) (mLat13 * 1E6), (int) (mLon13 * 1E6));
    GeoPoint p14 = new GeoPoint((int) (mLat14 * 1E6), (int) (mLon14 * 1E6));
    GeoPoint p15 = new GeoPoint((int) (mLat15 * 1E6), (int) (mLon15 * 1E6));
    GeoPoint p16 = new GeoPoint((int) (mLat16 * 1E6), (int) (mLon16 * 1E6));
    GeoPoint p17 = new GeoPoint((int) (mLat17 * 1E6), (int) (mLon17 * 1E6));
    GeoPoint p18 = new GeoPoint((int) (mLat18 * 1E6), (int) (mLon18 * 1E6));
    GeoPoint p19 = new GeoPoint((int) (mLat19 * 1E6), (int) (mLon19 * 1E6));
    GeoPoint p20 = new GeoPoint((int) (mLat20 * 1E6), (int) (mLon20 * 1E6));
    GeoPoint p21 = new GeoPoint((int) (mLat21 * 1E6), (int) (mLon21 * 1E6));
    GeoPoint p22 = new GeoPoint((int) (mLat22 * 1E6), (int) (mLon22 * 1E6));
    GeoPoint p23 = new GeoPoint((int) (mLat23 * 1E6), (int) (mLon23 * 1E6));
    GeoPoint p24 = new GeoPoint((int) (mLat24 * 1E6), (int) (mLon24 * 1E6));
    GeoPoint p25 = new GeoPoint((int) (mLat25 * 1E6), (int) (mLon25 * 1E6));
    GeoPoint p26 = new GeoPoint((int) (mLat26 * 1E6), (int) (mLon26 * 1E6));
    GeoPoint p27 = new GeoPoint((int) (mLat27 * 1E6), (int) (mLon27 * 1E6));
    GeoPoint p28 = new GeoPoint((int) (mLat28 * 1E6), (int) (mLon28 * 1E6));
    GeoPoint p29 = new GeoPoint((int) (mLat29 * 1E6), (int) (mLon29 * 1E6));
    GeoPoint p30 = new GeoPoint((int) (mLat30 * 1E6), (int) (mLon30 * 1E6));
    GeoPoint p31 = new GeoPoint((int) (mLat31 * 1E6), (int) (mLon31 * 1E6));
    GeoPoint p32 = new GeoPoint((int) (mLat32 * 1E6), (int) (mLon32 * 1E6));
    GeoPoint p33 = new GeoPoint((int) (mLat33 * 1E6), (int) (mLon33 * 1E6));
    GeoPoint p34 = new GeoPoint((int) (mLat34 * 1E6), (int) (mLon34 * 1E6));
    GeoPoint p35 = new GeoPoint((int) (mLat35 * 1E6), (int) (mLon35 * 1E6));
    GeoPoint p36 = new GeoPoint((int) (mLat36 * 1E6), (int) (mLon36 * 1E6));
    GeoPoint p37 = new GeoPoint((int) (mLat37 * 1E6), (int) (mLon37 * 1E6));
    GeoPoint p38 = new GeoPoint((int) (mLat38 * 1E6), (int) (mLon38 * 1E6));
    private PopupOverlay pop = null;
    GeoPoint point;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        mBMapMan = new BMapManager(getApplication());
        mBMapMan.init(null);
        //注意：请在试用setContentView前初始化BMapManager对象，否则会报错
        setContentView(R.layout.activity_main);
        kw_text = (EditText) findViewById(R.id.Kw_text);
        mMapView = (MapView) findViewById(R.id.bmapView);
        mMapView.setBuiltInZoomControls(true);
        //设置启用内置的缩放控件
        final MapController mMapController = mMapView.getController();
        ImageButton izdax2 = (ImageButton) findViewById(R.id.Izdax1);
        izdax2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                GeoPoint point = new GeoPoint((int) (43.743995 * 1E6), (int) (87.636164 * 1E6));
                //用给定的经纬度构造一个GeoPoint，单位是微度 (度 * 1E6)
                //准备overlay图像数据，根据实情情况修复
                Drawable mark = getResources().getDrawable(R.drawable.icon);
                Drawable mark2 = getResources().getDrawable(R.drawable.izdax);
//用OverlayItem准备Overlay数据
                OverlayItem item21 = new OverlayItem(p21, "item21", "item21");
                item21.setMarker(mark);
                OverlayItem item22 = new OverlayItem(p22, "item22", "item22");
                item22.setMarker(mark2);
                OverlayItem item23 = new OverlayItem(p23, "item23", "item23");
                item23.setMarker(mark2);
                OverlayItem item24 = new OverlayItem(p24, "item24", "item24");
                item24.setMarker(mark2);
                OverlayItem item25 = new OverlayItem(p25, "item25", "item25");
                item25.setMarker(mark2);
                OverlayItem item26 = new OverlayItem(p26, "item26", "item26");
                item26.setMarker(mark2);
                OverlayItem item27 = new OverlayItem(p27, "item27", "item27");
                item27.setMarker(mark2);
                OverlayItem item28 = new OverlayItem(p28, "item28", "item28");
                item28.setMarker(mark2);
                OverlayItem item29 = new OverlayItem(p29, "item29", "item29");
                item29.setMarker(mark2);
                OverlayItem item30 = new OverlayItem(p30, "item30", "item30");
                item30.setMarker(mark2);
//创建IteminizedOverlay
                OverlayTest itemOverlay = new OverlayTest(mark, mMapView);
//将IteminizedOverlay添加到MapView中
                mMapView.getOverlays().clear();
                mMapView.getOverlays().add(itemOverlay);
//现在所有准备工作已准备好，使用以下方法管理overlay.
//添加overlay, 当批量添加Overlay时使用addItem(List<OverlayItem>)效率更高
                itemOverlay.addItem(item21);
                itemOverlay.addItem(item22);
                itemOverlay.addItem(item23);
                itemOverlay.addItem(item24);
                itemOverlay.addItem(item25);
                itemOverlay.addItem(item26);
                itemOverlay.addItem(item27);
                itemOverlay.addItem(item28);
                itemOverlay.addItem(item29);
                itemOverlay.addItem(item30);
                mMapView.refresh();
                mMapController.setCenter(point);//设置地图中心点
                mMapController.setZoom(16);//设置地图zoom级别
            }
        });
        ImageButton izdax4 = (ImageButton) findViewById(R.id.Izdax3);
        izdax4.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                GeoPoint point = new GeoPoint((int) (43.826793 * 1E6), (int) (87.589358 * 1E6));
                //用给定的经纬度构造一个GeoPoint，单位是微度 (度 * 1E6)
                //准备overlay图像数据，根据实情情况修复
                Drawable mark = getResources().getDrawable(R.drawable.icon);
//用OverlayItem准备Overlay数据
                OverlayItem item31 = new OverlayItem(p31, "item31", "item31");
                item31.setMarker(mark);
                OverlayItem item32 = new OverlayItem(p32, "item32", "item32");
                item32.setMarker(mark);
                OverlayItem item33 = new OverlayItem(p33, "item33", "item33");
                item33.setMarker(mark);
                OverlayItem item34 = new OverlayItem(p34, "item34", "item34");
                item34.setMarker(mark);
                OverlayItem item35 = new OverlayItem(p35, "item35", "item35");
                item35.setMarker(mark);
                OverlayItem item36 = new OverlayItem(p36, "item36", "item36");
                item36.setMarker(mark);
                OverlayItem item37 = new OverlayItem(p37, "item37", "item37");
                item37.setMarker(mark);
                OverlayItem item38 = new OverlayItem(p38, "item38", "item38");
                item38.setMarker(mark);
//创建IteminizedOverlay
                OverlayTest itemOverlay = new OverlayTest(mark, mMapView);
//将IteminizedOverlay添加到MapView中
                mMapView.getOverlays().clear();
                mMapView.getOverlays().add(itemOverlay);

//现在所有准备工作已准备好，使用以下方法管理overlay.
//添加overlay, 当批量添加Overlay时使用addItem(List<OverlayItem>)效率更高
                itemOverlay.addItem(item31);
                itemOverlay.addItem(item32);
                itemOverlay.addItem(item33);
                itemOverlay.addItem(item34);
                itemOverlay.addItem(item35);
                itemOverlay.addItem(item36);
                itemOverlay.addItem(item37);
                itemOverlay.addItem(item38);
                mMapView.refresh();
                mMapController.setCenter(point);//设置地图中心点
                mMapController.setZoom(16);//设置地图zoom级别
            }
        });


        ImageButton izdax3 = (ImageButton) findViewById(R.id.Izdax2);


        izdax3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                GeoPoint point = new GeoPoint((int) (43.772078 * 1E6), (int) (87.622704 * 1E6));


//准备overlay图像数据，根据实情情况修复
                Drawable mark = getResources().getDrawable(R.drawable.icon);
//用OverlayItem准备Overlay数据
                OverlayItem item1 = new OverlayItem(p1, "item1", "item1");
                item1.setMarker(mark);
                OverlayItem item2 = new OverlayItem(p2, "item2", "item2");
                item2.setMarker(mark);
                OverlayItem item3 = new OverlayItem(p3, "item3", "item3");
                item3.setMarker(mark);
                OverlayItem item4 = new OverlayItem(p4, "item3", "item3");
                item4.setMarker(mark);
                OverlayItem item5 = new OverlayItem(p5, "item3", "item3");
                item5.setMarker(mark);
                OverlayItem item6 = new OverlayItem(p6, "item3", "item3");
                item6.setMarker(mark);
                OverlayItem item7 = new OverlayItem(p7, "item3", "item3");
                item7.setMarker(mark);
                OverlayItem item8 = new OverlayItem(p8, "item3", "item3");
                item8.setMarker(mark);
                OverlayItem item9 = new OverlayItem(p9, "item3", "item3");
                item9.setMarker(mark);
                OverlayItem item10 = new OverlayItem(p10, "item3", "item3");
                item10.setMarker(mark);
                OverlayItem item11 = new OverlayItem(p11, "item3", "item3");
                item11.setMarker(mark);
                OverlayItem item12 = new OverlayItem(p12, "item3", "item3");
                item12.setMarker(mark);
                OverlayItem item13 = new OverlayItem(p13, "item3", "item3");
                item13.setMarker(mark);
                OverlayItem item14 = new OverlayItem(p14, "item3", "item3");
                item14.setMarker(mark);
                OverlayItem item15 = new OverlayItem(p15, "item3", "item3");
                item15.setMarker(mark);
                OverlayItem item16 = new OverlayItem(p16, "item3", "item3");
                item16.setMarker(mark);
                OverlayItem item17 = new OverlayItem(p17, "item3", "item3");
                item17.setMarker(mark);
                OverlayItem item18 = new OverlayItem(p18, "item3", "item3");
                item18.setMarker(mark);
                OverlayItem item19 = new OverlayItem(p19, "item3", "item3");
                item19.setMarker(mark);
                OverlayItem item20 = new OverlayItem(p20, "item3", "item3");
                item20.setMarker(mark);


//创建IteminizedOverlay
                OverlayTest itemOverlay = new OverlayTest(mark, mMapView);
//将IteminizedOverlay添加到MapView中
                mMapView.getOverlays().clear();
                mMapView.getOverlays().add(itemOverlay);

//现在所有准备工作已准备好，使用以下方法管理overlay.
//添加overlay, 当批量添加Overlay时使用addItem(List<OverlayItem>)效率更高
                itemOverlay.addItem(item1);
                itemOverlay.addItem(item2);
                itemOverlay.addItem(item3);
                itemOverlay.addItem(item4);
                itemOverlay.addItem(item5);
                itemOverlay.addItem(item6);
                itemOverlay.addItem(item7);
                itemOverlay.addItem(item8);
                itemOverlay.addItem(item9);
                itemOverlay.addItem(item10);
                itemOverlay.addItem(item11);
                itemOverlay.addItem(item12);
                itemOverlay.addItem(item13);
                itemOverlay.addItem(item14);
                itemOverlay.addItem(item15);
                itemOverlay.addItem(item16);
                itemOverlay.addItem(item17);
                itemOverlay.addItem(item18);
                itemOverlay.addItem(item19);
                itemOverlay.addItem(item20);
                mMapView.refresh();
                //用给定的经纬度构造一个GeoPoint，单位是微度 (度 * 1E6)
                mMapController.setCenter(point);//设置地图中心点
                mMapController.setZoom(16);//设置地图zoom级别
            }
        });

        ImageButton izdax = (ImageButton) findViewById(R.id.Izdax);
        izdax.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Log.e("Onclick", "Clicked");


                String keyword = kw_text.getText().toString().trim();
                if (keyword.length() == 0) {

                    Toast.makeText(Main.this, "请输入查询关键词！", Toast.LENGTH_SHORT).show();
                    kw_text.requestFocus();
                    return;
                } else {
                    mMKSearch = new MKSearch();
                    mMKSearch.init(mBMapMan, new MySearchListener());
//                    mMKSearch.poiSearchInCity("乌鲁木齐", "新疆大学");
                    mMKSearch.poiSearchNearBy(keyword, new GeoPoint((int) (43.772078 * 1E6), (int) (87.622704 * 1E6)), 500);
                }

//                PopupOverlay pop = new PopupOverlay(mMapView, new PopupClickListener() {
//                    @Override
//                    public void onClickedPopup(int index) {
//                        //在此处理pop点击事件，index为点击区域索引,点击区域最多可有三个
//                    }
//                });

            }

        });

        // 得到mMapView的控制权,可以用它控制和驱动平移和缩放
        //87.622704,43.772078
        point = new GeoPoint((int) (43.800834 * 1E6), (int) (87.637427 * 1E6));
        //用给定的经纬度构造一个GeoPoint，单位是微度 (度 * 1E6)
        //准备overlay图像数据，根据实情情况修复
        Drawable mark = getResources().getDrawable(R.drawable.icon);
//用OverlayItem准备Overlay数据
        OverlayItem item00 = new OverlayItem(p00, "item31", "item31");
        item00.setMarker(mark);
        OverlayItem item01 = new OverlayItem(p01, "item32", "item32");
        item01.setMarker(mark);
        OverlayItem item02 = new OverlayItem(p02, "item33", "item33");
        item02.setMarker(mark);
//创建IteminizedOverlay
        OverlayTest itemOverlay = new OverlayTest(mark, mMapView);
//将IteminizedOverlay添加到MapView中
        mMapView.getOverlays().clear();
        mMapView.getOverlays().add(itemOverlay);

//现在所有准备工作已准备好，使用以下方法管理overlay.
//添加overlay, 当批量添加Overlay时使用addItem(List<OverlayItem>)效率更高
        itemOverlay.addItem(item00);
        itemOverlay.addItem(item01);
        itemOverlay.addItem(item02);
        mMapView.refresh();
        mMapController.setCenter(point);//设置地图中心点
        mMapController.setZoom(13);//设置地图zoom级别

    }


    @Override
    protected void onDestroy() {
        mMapView.destroy();
        if (mBMapMan != null) {
            mBMapMan.destroy();
            mBMapMan = null;
        }
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        mMapView.onPause();
        if (mBMapMan != null) {
            mBMapMan.stop();
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        mMapView.onResume();
        if (mBMapMan != null) {
            mBMapMan.start();
        }
        super.onResume();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.ping_mian) {
            mMapView.setSatellite(false);
        } else if (item.getItemId() == R.id.menu_3d) {
            mMapView.setSatellite(true);
        }
        return true;
    }

    public class MySearchListener implements MKSearchListener {

        @Override
        public void onGetAddrResult(MKAddrInfo arg0, int arg1) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onGetBusDetailResult(MKBusLineResult arg0, int arg1) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onGetDrivingRouteResult(MKDrivingRouteResult arg0, int arg1) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onGetPoiDetailSearchResult(int arg0, int arg1) {
            // TODO Auto-generated method stub

        }

        public void onGetPoiResult(MKPoiResult res, int type, int error) {

            if (error == MKEvent.ERROR_RESULT_NOT_FOUND) {
                Toast.makeText(Main.this, "抱歉，未找到结果", Toast.LENGTH_LONG).show();
                return;
            } else if (error != 0 || res == null) {
                Toast.makeText(Main.this, "搜索出错啦..", Toast.LENGTH_LONG).show();
                return;
            }
            // 将poi结果显示到地图上
            PoiOverlay poiOverlay = new PoiOverlay(Main.this, mMapView);
            poiOverlay.setData(res.getAllPoi());
            mMapView.getOverlays().clear();
            mMapView.getOverlays().add(poiOverlay);
            mMapView.refresh();
            //当ePoiType为2（公交线路）或4（地铁线路）时， poi坐标为空
            for (MKPoiInfo info : res.getAllPoi()) {
                if (info.pt != null) {
                    mMapView.getController().animateTo(info.pt);
                    break;
                }
            }
        }

        @Override
        public void onGetShareUrlResult(MKShareUrlResult arg0, int arg1, int arg2) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onGetSuggestionResult(MKSuggestionResult arg0, int arg1) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onGetTransitRouteResult(MKTransitRouteResult arg0, int arg1) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onGetWalkingRouteResult(MKWalkingRouteResult arg0, int arg1) {
            // TODO Auto-generated method stub

        }

    }

    public class OverlayTest extends ItemizedOverlay {
        public OverlayTest(Drawable arg0, MapView arg1) {
            super(arg0, arg1);
        }

        @Override
        protected boolean onTap(int arg0) {
            //弹出覆盖物
            showPopupOverlay(getItem(arg0).getPoint());
            return true;
        }

        @Override
        public boolean onTap(GeoPoint arg0, MapView arg1) {
            if (pop != null) {
                //隐藏覆盖物
                pop.hidePop();
            }
            return false;
        }
    }

    // 弹出覆盖物
    public void showPopupOverlay(GeoPoint p1) {
        pop = new PopupOverlay(mMapView, null);
        View view = getLayoutInflater().inflate(R.layout.pop_layout, null);

        ImageView scenary = (ImageView) view.findViewById(R.id.user_logo);
        if (p1.getLongitudeE6() == mLon00 && p1.getLatitudeE6() == mLat00) {
            scenary.setImageResource(R.drawable.izdax);
        }

        View pop_layout = view.findViewById(R.id.pop_layout);


        //将View转化成用于显示的bitmap
        Bitmap[] bitMaps = {BMapUtil.getBitmapFromView(pop_layout)};
        pop.showPopup(bitMaps, p1, 32);
    }
}
